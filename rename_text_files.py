import constants
import os
import pathlib


for path, dirs, file in os.walk(constants.unzipping_dir):
    if "processed.txt" in file:
        new_name = pathlib.Path(path).parts[-1] + '.txt'
        os.rename(os.path.join(path, "processed.txt"), os.path.join('IKC_TEXT_FILES', new_name))
        print(new_name)
