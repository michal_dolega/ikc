import constants
import os
import urllib.request
import csv
import sys
import pprint
from multiprocessing import Pool

def download_single_issue(csv_line):
    id = csv_line[1]
    dirs_path = os.path.join(constants.publication ,csv_line[4], csv_line[2])
    file_name = csv_line[3] + '.zip'
    if not os.path.exists(dirs_path):
        os.makedirs(dirs_path)
    if file_name not in os.listdir(dirs_path):
        url = constants.collection_address.format(id)
        try:
            urllib.request.urlretrieve(url, os.path.join(dirs_path, file_name))
            print("Item with id: {} successfully downloaded".format(id))
        except Exception as inst:
            print("Something went wrong")
            print(inst)

def generate_csv_iterator(file):
    with open(file, 'r', encoding='utf-8') as csv_file:
        csv_list =  list(csv.reader(csv_file))[1:]
        final_list = []
        for i in csv_list:
            if check_date(i[4], 1918, 1939):
                final_list.append(i)
        return final_list

def check_date(text, begining, end):
  for year in [str(x) for x in range(begining, end+1)]:
    if year in text:
      return True


if __name__ == "__main__":
    items = generate_csv_iterator(constants.csv_file)
    # pprint.pprint(items)
    with Pool(processes=20) as workers:
        workers.map(download_single_issue, items)
