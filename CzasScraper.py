# -*- coding: utf-8 -*-
import scrapy
import re

def check_date(text, begining, end):
  for year in [str(x) for x in range(begining, end+1)]:
    if year in text:
      return True

class Issue(scrapy.Item):
    year = scrapy.Field()
    month = scrapy.Field()
    name = scrapy.Field()
    address = scrapy.Field()
    id = scrapy.Field()


class MultipleQuotesSpider(scrapy.Spider):
    name = "IKC"
    allowed_domains = ["http://mbc.malopolska.pl/"]
    start_urls = ['http://mbc.malopolska.pl/dlibra/publication?id=20747&tab=3']

    def parse(self, response):

        my_selector = response.css('div[id = Structure]')
        sub_selector = my_selector.css('li')
        with open('output.txt', 'w') as a:
            for ul_selector in sub_selector.css('ul'):
                for item in ul_selector.css('a[class = item-content]'):
                    issue = Issue()
                    name = item.css('a[class = item-content]::text').extract_first()
                    issue['year'] = name.strip()
                    if check_date(issue['year'], 1918, 1939):
                        address = item.css('a[class = item-content]::attr(href)').extract_first()
                        a.write(name.strip() + '\t' + address.strip() + '\n')
                        yield scrapy.Request(url=address, callback=self.parse2, meta={'item' : issue}, dont_filter=True)


    def parse2(self, response):
        foo = response.meta['item']
        with open('output2.txt', 'a') as months:
            for item in response.css('#struct > ul > li > ul > li > ul'):
                name = item.css('a[class = item-content]::text').extract_first().strip()
                address = item.css('a[class = item-content]::attr(href)').extract_first()
                months.write(name.strip() + address.strip() + '\n')
                new_item = Issue(foo)
                new_item['month'] = name
                yield scrapy.Request(url=address, callback=self.parse3, meta={'item': new_item}, dont_filter=True)

    def parse3(self, response):
        foo = response.meta['item']
        with open('output3.txt', 'a') as issues:
            for item in response.css('#struct > ul > li > ul > li > ul > li > ul'):
                address = item.css('a:nth-child(2)::attr(href)').extract_first()
                name = item.css('a[class = contentTriggerStruct]::attr(title)').extract_first()[:-3]
                issue = Issue(foo)
                issue['name'] = name
                yield scrapy.Request(url=address, callback=self.parse4, meta={'item': issue}, dont_filter=True)

    def parse4(self, response):
        foo = response.meta['item']
        foo['address'] = response.css('#EditionsList > script::text').re(r"'(.+)'")[0]
        print(foo['address'])
        regex = re.compile(r'\d+')
        match = re.search(regex, foo['address'])
        id = match.group(0)
        foo['id'] = id
        yield foo
