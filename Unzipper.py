import os
import zipfile
import constants
from pathlib import Path

for path, subdir, file in os.walk(constants.publication):
    for element in file:
        if os.path.splitext(element)[1] == ".zip":
            new_path = Path(os.path.join(path, os.path.splitext(element)[0])).parts
            new_path = os.path.join(constants.unzipping_dir, *new_path[1:])
            print("new path is " + str(new_path))
            if not os.path.exists(new_path):
                os.makedirs(new_path)
                try:
                    with zipfile.ZipFile(os.path.join(path, element), "r") as zip_ref:
                        zip_ref.extractall(path=new_path)
                except:
                    continue
