import os
import re
from pathlib import Path

def extract_txt_layer():
    for path, subdir, file in os.walk('.'):
        if "index.djvu" in file and "tekst.txt" not in file:
            command = "djvutxt " + "\"" + os.path.join(path, "index.djvu") + "\"" + " " + "\"" + os.path.join(path, "tekst.txt") + "\""
            print(command)
            os.system(command)

def reshape_text_layer():
    for path, subdir, file in os.walk('.'):
        if "tekst.txt" in file and "processed.txt" not in file:
            with open (os.path.join(path, "tekst.txt"), 'r', encoding="utf8") as file:
                print(file.name)
                text_to_process = file.read()
                regex_tool = re.compile(r"[^\wćąźżęłśŻŹŁŚ_ĆĄĘóÓńŃ]")
                processed_text = re.sub(regex_tool, "", text_to_process)
                with open(os.path.join(path, "processed.txt"), 'w', encoding="utf8") as output:
                    output.write(processed_text)

def count_tyfus_expression():
    counter = 0
    for path, subdir, file in os.walk('.'):
        if "processed.txt" in file:
            with open(os.path.join(path, "processed.txt"), 'r', encoding="utf8") as file:
                text = file.read()
                if "tyfus" in text:
                    counter += 1
    return counter

def create_directories_list():
    directories = set()
    for path, subdir, file in os.walk('.'):
        if "processed.txt" in file:
            directories.add(Path(path).parts[-1])
    print(list(directories))


def open_file():
    pass

reshape_text_layer()