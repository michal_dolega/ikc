# -*- coding: utf-8 -*-

publication = "IKC"
csv_file = "newIKC.csv"
collection_address = 'http://mbc.malopolska.pl/Content/{}/zip/'
unzipping_dir = "IKC_UNZIPPED"
text_data_dir = "IKC_TEXT_FILES"
data_csv = "IKCdata.csv"
words_to_check = ['tyfus', 'plamisty', 'tyfusplamisty']
words_to_check_keys = [i + '_presence' for i in words_to_check]


