import os
import re
import csv
import constants

KEYS = ['full_name', 'year', 'issue', 'month', 'day', 'full_date', *constants.words_to_check_keys]

DATA_REGEX = r'(\d{4}).+ (\d+).+\((\d+) (\w+)\)'

MONTH_TRANSLATION_DICT = {'I': '01',
                         'II': '02',
                         'III': '03',
                         'IV': '04',
                         'V': '05',
                         'VI': '06',
                         'VII': '07',
                         'VIII': '08',
                         'IX': '09',
                         'X': '10',
                         'XI': '11',
                         'XII': '12'}


def convert_roman_months_to_latin(roman_num):
    return MONTH_TRANSLATION_DICT[roman_num]


def is_word_present_in_string(string, word):
        return word in string


def get_issue_name(filename):
    return os.path.splitext(filename)[0]


def get_date_data(filename):
    try:
        year, issue, day, month = re.search(re.compile(DATA_REGEX), filename).groups()
        month = convert_roman_months_to_latin(month)
        full_date = '{}.{}.{}'.format(year, month, day)
        return [year, issue, month, day, full_date]
    except Exception as err:
        print("Something went wrong with: {}\nException: {}".format(filename, err))


def get_words_presence_data(filename, list_of_words):
    text_data_dir = constants.text_data_dir
    with open(os.path.join(text_data_dir, filename), encoding="utf8") as file:
        data_to_search = file.read()
        return [is_word_present_in_string(data_to_search, word) for word in list_of_words]


def values_list_generator(filename_to_parse):
    full_name = get_issue_name(filename_to_parse)
    date_data = get_date_data(filename_to_parse)
    words_presence = get_words_presence_data(filename_to_parse, constants.words_to_check)
    values_list = [full_name, *date_data, *words_presence]
    return values_list


def generate_issue_dict(filename):
    values = values_list_generator(filename)
    return dict(zip(KEYS, values))


def get_list_of_data_dicts():
    list_of_files = os.listdir(constants.text_data_dir)
    return list(map(generate_issue_dict, list_of_files))


def dict_to_csv(dicts_list):
    with open(constants.data_csv, 'w', encoding='utf-8') as file:
        writer = csv.DictWriter(file, dicts_list[0].keys())
        writer.writeheader()
        writer.writerows(dicts_list)

def generate_data_csv():
    data_dicts_list = get_list_of_data_dicts()
    dict_to_csv(data_dicts_list)

generate_data_csv()